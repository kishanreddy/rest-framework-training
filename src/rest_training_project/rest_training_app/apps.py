from django.apps import AppConfig


class RestTrainingAppConfig(AppConfig):
    name = 'rest_training_app'
